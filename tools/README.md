## Howto

1. Double-click on 'Blog Screenshot' to install it as a system-level service.
2. Open system preferences -> Keyboard -> Shortcuts and assign a keystroke to
   it.

## Assumptions

- Blog is expected to exist in "$HOME/code/go/blog"
- Image assets are stored to "$BLOGHOME/static/img/<name>.png"
- iTerm is installed, and a vim terminal in INPUT mode is available to enter the
  markdown image stuff into. (the script switches to iterm and inputs
  `![<name>](/img/<name>.png)`
