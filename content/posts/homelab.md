---
title: "Homelab"
date: 2017-09-21T22:05:31-05:00
type: post
---

_Note: my homelab has changed significantly since this post was initially...
posted. Updates will come along with new pictures._

IMAGES COMING SOON!

#### The Setup

Hello loyal followers. Apparently I'm down with the sickness (Pneumonia), and I figured _what better way to spend my time than detailing my home-lab setup, eh?_

Also, I just learned markdown last night so it's time to practice.

Without much further blabbering, I'll get right down to business, starting with the worlds most horrible Inkscape:

![Arkham City](internalnetworkdiagram.png)

If you're a board game player, you might recognize a few of the names above. Ahem.

![Arkham Nerds](arkham_horror_06_grande.jpg)

Also: eff batman for making Arkham City sound a lot less horrifying and a lot more twelve-year-old-esque.

---

#### Not So Specifics

**anotherdimension** used to be a pfSense box - but I figured I'd need to learn pf eventually, and so I did. [It wasn't that hard.](http://www.openbsd.org/faq/pf/)

**trainstation** does not support HTTPS. I didn't know this going in, and it's the only thing I dislike about the switch.

**abyss and library** are both homemade machines using spare parts/random stuff from Microcenter. You wouldn't believe how annoying the proprietary _SFX_ PSU form factor is.

Hint: **REALLY** annoying.

**library** was _really really_ easy to set up. Good job FreeNAS dudes.

**abyss** was a troublesome beast - I first had trouble with bonding (wound up using teaming instead), followed by some SELinux woes. I'll go over all of that in this blog post. This Server is responsible for running all of my virtual machines (think ESXI) and uses the storage of my FreeNAS box (library) to do so.

**time and space** were very simple to setup - but it's worth noting that OpenBSD _deprecated_ BIND support in 5.7. It took me literally hours to figure this out. I am not a smart man.

**dexterdrake** is primarily used to host my keepass database. Keepass does **NOT** play well with samba/cifs. Know this. I also store my master git branch here.

---

#### Specifics

Yeah, I know this is what you want. I'll organize my specifics by server and throw down all of the configs that I struggled with. From the top:

##### anotherdimension

-will complete in the future.

##### trainstation

Nothing too noteworthy here. Change the default **user password**. Enabling LACP is just this simple:
![switchlacp](Screenshot-from-2015-09-17-21-46-51.png)
Uh, and save your config.

##### library

No tricks here, the FreeNAS installer does almost everything for you, including provisioning of a bootable USB device. Follow the installer and read all of the text that appears - it's dead simple.

Things that I'm doing with library.arkham.city include:

1. 2 AFP shares for 2 timemachine backups
2. 1 giant CIFS share for media purposes
3. 1 NFS share for all of the VMs on abyss.

Permissions are as follows:

    ls -l /mnt/zpool/
    drwxr-xr-x  4 jolson    wheel   air-timemachine
    drwxr-xr-x  3 root      wheel   kvm
    drwxrwxr-x+ 8 root      cows    losershare
    drwxr-xr-x  4 jkoehler  wheel   oldbook-timemachine

jkoehler is my roommate - naturally we both belong to the 'cows' group so that we have access to our treasured media on _losershare_. _kvm_ is of course the virtual machine NFS share.

For your viewing pleasure:

![yesviewingpleasure](Screenshot-from-2015-09-21-22-05-18.png)
![pleasure2](Screenshot-from-2015-09-21-22-06-42.png)

##### abyss

Abyss was a little more arduous.

I used a CentOS 7 DVD to install the 'Virtual Machine Manager' package. I opted to make it headless, since I wouldn't use the GUI anyway. This was straightforward.

_All of the VMs on this box are actually living on the FreeNAS box._ That's probably the coolest thing about my homelab. NFS has been rock solid for me.

My next task was getting my network bond to work properly. I also needed a bridge so that my VMs could rest on the real subnet. Bonding is being deprecated, so I opted to use teaming. My hard-earned config scripts are below:

**eth0 (or enp1s0f0, if you prefer):**

    cat ifcfg-enp1s0f0
    NAME=enp1s0f0
    DEVICE=enp1s0f0
    DEVICETYPE=TeamPort
    BOOTPROTO=none
    ONBOOT=no
    NM_CONTROLLED=no
    TEAM_MASTER=team0
    TEAM_PORT_CONFIG='{"prio":100}'

**eth1:**

    cat ifcfg-enp1s0f1
    NAME=enp1s0f1
    DEVICE=enp1s0f1
    DEVICETYPE=TeamPort
    BOOTPROTO=none
    ONBOOT=no
    NM_CONTROLLED=no
    TEAM_MASTER=team0
    TEAM_PORT_CONFIG='{"prio":100}'

**team0:**

    cat ifcfg-team0
    DEVICE=team0
    NAME=team0
    DEVICETYPE=Team
    ONBOOT=yes
    USERCTL=no
    BOOTPROTO=none
    NM_CONTROLLED=no
    TEAM_CONFIG='{"runner":{"name":"lacp"},"link_watch": {"name":"ethtool"}}'
    BRIDGE=virbr666
**Note** how the bridge is defined in the team0 script. 666 is an arbitrary number - I'd use anything but '0', since KVM/qemu tend to suck up virbr0. Also, I use lacp - you're free to use round-robin or whatever you'd like under the 'runner' section.

**bridge:**

    cat ifcfg-virbr666
    DEVICE=virbr666
    ONBOOT=YES
    TYPE=Bridge
    DELAY=0
    BOOTPROTO=static
    IPADDR=192.168.x.x
    NETMASK=255.255.255.0
    DNS1=8.8.8.8
    DNS2=8.8.4.4
    GATEWAY=192.168.x.1

[**Noice.**](https://www.youtube.com/watch?v=a8c5wmeOL9o) Reboot the server and see if it works.


With regards to NFS, I decided to mount the FreeNAS share directly to the expected libvirt directory to avoid too many SELinux complications.

**Installing NFS utilities**:

    yum install nfs-utils

**Fstab**:

    grep nfs /etc/fstab
    library:/mnt/zpool/kvm    /var/lib/libvirt/images/freenas-pool nfs    rsize=8192,wsize=8192,timeo=14,intr

It took me a hot second to find the SELinux boolean that allowed NFS mounts. Here it is, in all its glory:

    setsebool -P virt_use_nfs 1

Voila, working VMs over a network share. A dream come true.

I manage all of my VMs remotely using virt-manager. In most distros, virt-manager is a pretty straightforward thing:

    sudo dnf install virt-manager

It's pretty badass once you're done. Should look something like this:

![prettyvmsandshit](Screenshot-from-2015-09-21-21-47-55-2.png)

##### time

Ah, DNS - I opted to use OpenBSD because I want my DNS server to be stable and secure. And really small. OpenBSD fits the bill. I pieced together about ten billion tutorials. This is what I learned.

As i mentioned before, **BIND is deprecated** - it has essentially been split in half - welcome nsd and unbound.

After getting my ip set up, I opted to do nsd first since it looked the most complex - it turns out I was correct.

Why is everything in /var/? Not a clue. OpenBSD is weird.

Note that this is my master DNS server - this is how my config files look. (Also, I just realized that NSD is an anagram for DNS).

**Master NSD Configuration**

    cat /var/nsd/etc/nsd.conf
    server:
        ip-address: 192.168.x.x
        hide-version: yes
        do-ip4: yes
        port: 8053
        zonesdir: "/var/nsd/zones"
        logfile: "/var/log/nsd.log"

    remote-control:
        control-enable: yes
        control-interface: 127.0.0.1
        control-port: 8952
        server-key-file: "/var/nsd/etc/nsd_server.key"
        server-cert-file: "/var/nsd/etc/nsd_server.pem"
        control-key-file: "/var/nsd/etc/nsd_control.key"
        control-cert-file: "/var/nsd/etc/nsd_control.pem"

    key:
        name: "dnskey"
        algorithm: hmac-sha256
        secret: "changeme"

    pattern:
        name: "toslave"
        notify: 192.168.x.x dnskey
        provide-xfr: 192.168.x.x dnskey

    zone:
        name: "arkham.city"
        zonefile: "arkham.city.zone"
        include-pattern: "toslave"

    zone:
        name: "x.168.192.in-addr.arpa"
        zonefile: "192.168.x.zone"
        include-pattern: "toslave"

Note that anywhere I've used an 'x' above should be changed to suite your environment. This will set up your master to be authoritative over the 'arkham.city' domain and also the appropriate reverse-lookup domain. You'll need to generate your own cert-stuff as well.

The reason I have nsd listening on port 8053 is because unbound will be set up to query the nsd server for any forward or reverse lookups that pertain to the arkham.city domain. Unbound will listen on port 53 by default, so be sure to change the port that nsd listens on (also 53 by default). I arbitrarily selected 8053.

Also, your 'toslave' pattern should be pointed at the IP address of your slave server, naturally.

The zone files are identical to BIND zone files, but here they are anyway.

**NSD zone files**

    cat /var/nsd/zones/arkham.city.zone
    $ORIGIN arkham.city.
    $TTL 1800

    @       IN      SOA     time.arkham.city. admin.arkham.city. (
                        2015091701      ; serial
                        3600            ; refresh
                        900             ; retry
                        1209600         ; expire
                        1800            ; ttl
                        )

    ; Name servers
                        IN      NS      time.arkham.city.
                        IN      NS      space.arkham.city.

    ; Internal server names
    anotherdimension        IN      A       192.168.x.x
    trainstation            IN      A       192.168.x.x
    time                    IN      A       192.168.x.x
    space                   IN      A       192.168.x.x
    library                 IN      A       192.168.x.x
    abyss                   IN      A       192.168.x.x
    dexterdrake             IN      A       192.168.x.x
    theunnamable            IN      A       192.168.x.x
    cthulhu                 IN      A       192.168.x.x

    ; External server names
    blog                    IN      A       104.236.22.237

Note that I've included any external servers that use the arkham.city domain, as otherwise the lookups would fail.

Whenever you make a change to this zonefile, **UPDATE THE SERIAL NUMBER** or else the slave won't pick up on your changes. Change it to todays date + a revision number.

    cat /var/nsd/zones/192.168.x.zone
    $ORIGIN x.168.192.in-addr.arpa.
    $TTL 1800

    @       IN      SOA     time.arkham.city. admin.arkham.city. (
                        2015091701      ; serial
                        3600            ; refresh
                        900             ; retry
                        1209600         ; expire
                        1800            ; ttl
                        )

    ; Name servers
                        IN      NS      time.arkham.city.
                        IN      NS      space.arkham.city.

    ; Reverse server records
    x                       IN      PTR     anotherdimension.arkham.city.
    x                       IN      PTR     trainstation.arkham.city.
    x                       IN      PTR     time.arkham.city.
    x                       IN      PTR     space.arkham.city.
    x                       IN      PTR     library.arkham.city.
    x                       IN      PTR     abyss.arkham.city.
    x                       IN      PTR     dexterdrake.arkham.city.
    x                       IN      PTR     theunnamable.arkham.city.
    x                       IN      PTR     cthulhu.arkham.city.

Same story - replace the 'x' characters above with your own stuff. Also probably replace the server names and stuff too. _ANOTHER REMINDER TO UPDATE THE SERIAL NUMBER WHEN YOU MAKE CHANGES._

Ramble: When setting up the IP stuff for this server, it's fine to make it query itself for DNS resolution purposes:

    cat /etc/resolv.conf
    lookup file bind
    nameserver 127.0.0.1

Seriously, that's it? Yup. Just make sure nsd is up and running, and that it loads on boot.

    /etc/rc.d/nsd start
    rcctl enable nsd

**Unbound Configuration**

Would you believe me if I told you that it gets even easier?

Because it does. Unbound is stupidly easy.

    cat /var/unbound/etc/unbound.conf
    server:
        interface: 0.0.0.0
        interface: ::0

        access-control: 192.168.x.x/24 allow
        access-control: 10.2.x.x/25 allow
        access-control: 127.0.0.0/8 allow
        access-control: ::0/0 refuse
        access-control: ::1 allow

        hide-identity: yes
        hide-version: yes

        local-zone: "168.192.in-addr.arpa." nodefault

    remote-control:
        control-enable: yes
        control-interface: 127.0.0.1
        control-interface: ::1

    stub-zone:
        name: "arkham.city"
        stub-addr: 192.168.x.x@8053

    stub-zone:
        name: "x.168.192.in-addr.arpa."
        stub-addr: 192.168.x.x@8053

The above configuration is all that you need. For real. Set up the access-control parameters to allow particular subnets to query unbound. Set up your stub-zones to tell unbound to consult your local nsd instance for any arkham.city-based resolutions.

My access-control list also includes my 10.0.x.x VPN subnet, in case you were wondering what that was.

Do I understand everything above? Nope. Can I promise you that it's secure? nada. But it works for me, and it's fully functional. For my home network, it's more than enough.

Same story regarding starting it up:

    /etc/rc.d/unbound start
    rcctl enable unbound

That's all she wrote! It took me awhile to set this up, let me know if you have any trouble - I probably left something out.

If you make any changes to your nsd.conf file, I use the following procedure to apply the changes:

    /etc/rc.d/nsd reload
    /etc/rc.d/unbound restart

The changes will auto-magically apply to the slave server.

##### space

The slave DNS is almost identical, so I won't explain things in detail - instead I'll just vomit out some configs. Here you go.

**NSD stuff**

cat /var/nsd/etc/nsd.conf

    server:
        ip-address: 192.168.x.x
        hide-version: yes
        do-ip4: yes
        port: 8053
        zonesdir: "/var/nsd/zones"
        logfile: "/var/log/nsd.log"

    remote-control:
        control-enable: yes
        control-interface: 127.0.0.1
        control-port: 8952
        server-key-file: "/var/nsd/etc/nsd_server.key"
        server-cert-file: "/var/nsd/etc/nsd_server.pem"
        control-key-file: "/var/nsd/etc/nsd_control.key"
        control-cert-file: "/var/nsd/etc/nsd_control.pem"

    key:
        name: "dnskey"
        algorithm: hmac-sha256
        secret: "changeme"

    pattern:
        name: "frommaster"
        allow-notify: 192.168.x.x dnskey
        request-xfr: AXFR 192.168.x.x@8053 dnskey

    zone:
        name: "arkham.city"
        zonefile: "arkham.city.zone"
        include-pattern: "frommaster"

    zone:
        name: "x.168.192.in-addr.arpa"
        zonefile: "192.168.x.zone"
        include-pattern: "frommaster"

Change allow-notify and request-xfr to your master IP.

There are no zone files on the slave - they live in magic christmasland instead.

**unbound stuff**

    cat /var/unbound/etc/unbound.conf
    server:
        interface: 0.0.0.0
        interface: ::0

        access-control: 192.168.x.x/24 allow
        access-control: 10.2.x.x/25 allow
        access-control: 127.0.0.0/8 allow
        access-control: ::0/0 refuse
        access-control: ::1 allow

        hide-identity: yes
        hide-version: yes

        local-zone: "168.192.in-addr.arpa." nodefault

    remote-control:
        control-enable: yes
        control-interface: 127.0.0.1
        control-interface: ::1

    stub-zone:
        name: "arkham.city"
        stub-addr: 192.168.x.x@8053

    stub-zone:
        name: "1.168.192.in-addr.arpa."
        stub-addr: 192.168.x.x@8053

Holy CRAP I think you guys deserve a picture of this hot action:
![hot action](Screenshot-from-2015-09-21-22-25-46.png)


##### dexterdrake

This is the most simple server that I own. It serves two purposes:

1. SSHfs repository for my keepass database
2. Holds my git master branch

The neat thing about this is that FreeNAS is taking [snapshots](http://olddoc.freenas.org/index.php/Periodic_Snapshot_Tasks) every hour, so if my keepass DB/git branch is deleted for whatever reason I can always revert to my last snapshot to get it back. I've **definitely** never deleted my git repo by mistake. KVM naturally supports VM snapshots, but it's not like I have VM snapshots waiting around for when I accidentally brutalize important data of mine. ZFS is awesome.

Anyway, enough about ZFS.

**Git**

Simple enough, just initialize a repo on the server. I made a very [unprivileged](http://allanfeid.com/content/creating-chroot-jail-ssh-access) user called 'git' that I use to access my repo with.

    yum install git
    git init --bare jessefiles.git

That's all - now I can clone that repo to a local machine if I need to work on something:

    git clone ssh://git@dexterdrake.arkham.city/home/git/jessefiles.git

And when I'm done working on something, I can commit the changes back to dexterdrake, knowing that my files will sleep soundly:

    cd jessefiles
    vi dope_new_python_project.py
    git add dope_new_python_project.py
    git commit
    git push

**SSHfs for keepass**

Before I get ahead of myself, I'd like to mention that I initially wanted my  keepass DB to live on my SMB/CIFS share (losershare). My experience tells me that it _just doesn't work_. [This](https://www.keepassx.org/forum/viewtopic.php?t=1763) is a relevant post, 5 years in the making. Keep in mind that I'm not a definitive source of information regarding smb-keypass mountage, but it didn't work for me and I tried for awhile.

I then rethought my strategy of keeping all of my passwords on my semi-public media share, and decided that SSHfs was probably an overall better idea anyway. It's also pretty simple, and I'm a sucker for simple stuff.

I actually didn't need to do anything except make a directory on dexterdrake. I logged in as git and issued a single command:

    ssh git@dexterdrake
    mkdir keepass

Alright, now that we're through the gruelling process of getting the server configured, let's configure my workstation (Fedora 22):

    sudo dnf install sshfs
    sshfs git@192.168.1.7:/home/git/keepass ~/keepass

Annnd it's mounted! I made my keypass database and unmounted it to ensure that it worked properly.

    fusermount -uz ~/keepass

Hint: It did. Check out my sexy script at the end of this post if you want this process simplified even more.



##### cthulhu

-Will update this with additional informations once I decide which combination of Nagios stuff to bolt on to Core. At this point I'm thinking about starting with [Thruk](http://www.thruk.org/) and going from there.

Currently it's simply stock Core monitoring stuff and things.

---

#### Useful Scripts

I developed some useful scripts throughout the course of this project. I'd like to share some of those with you.

##### Mounting/Unmounting the sshfs share.

I use the following script to mount/unmount my sshfs share (on dexterdrake).

```
#!/bin/bash
read -r -p "Mount or Unmount? [M/u] " response
if [[ $response =~ ^([Mm][oO][Uu][nN][tT]|[mM])$ ]]; then
    printf "Mounting...\n"
    /usr/bin/sshfs git@dexterdrake:/home/git/keepass ~/keepass
elif [[ $response =~ ^([uU][nN][Mm][oO][Uu][nN][tT]|[uU])$ ]]; then
    printf "Unmounting...\n"
    /usr/bin/fusermount -uz ~/keepass
else
    printf "\nwrong letter damn good coffee.\n"
fi
```

Very helpful.




Thanks for reading mate.

LOVE,
JESSE
