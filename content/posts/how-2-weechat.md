---
title: "How 2 Weechat"
date: 2017-01-18T23:01:59-05:00
type: post
---

---
#### ohai

Since I always forget how to configure my damned weechat, I'll stick my guide in here (for your reference and for mine!)

First step is to _install_ weechat. Pretty straightforward on most distributions.

    pacman -S weechat

I make use of the urxvt + solarized theme for pretty much everything, therefore:

    pacman -S rxvt-unicode

http://ethanschoonover.com/solarized

Once the terminal is working properly, we have a lil' bit of weechat shit to get through.

Launch weechat and connect it to your ZNC bouncer if necessary:

    /server add BNC my.bouncer.net/6697 -ssl -username=username/freenode -password=password -autoconnect
    /connect BNC
    /save

I shameless stole this ingenius configuration: http://benoliver999.com/2014/02/18/weechatconf/

A big thanks to **Ben Oliver** for compiling this beautiful list.

I'm copying it here just in case that site ever goes down:

    /script install buffers.pl buffer_autoclose.py iset.pl go.py colorize_nicks.py
    /key bind meta-g /go
    /set weechat.bar.status.color_bg 0
    /set weechat.bar.title.color_bg 0
    /set weechat.color.chat_nick_colors 1,2,3,4,5,6
    /set buffers.color.hotlist_message_fg 7
    /set weechat.bar.buffers.position left
    /set buffers.look.indenting on
    /set irc.look.server_buffer independent
    /set weechat.bar.buffers.items buffers
    /set buffers.look.hide_merged_buffers server
    /set weechat.look.buffer_notify_default message
    /set irc.look.smart_filter on
    /filter add irc_smart * irc_smart_filter *
    /filter add irc_join_names * irc_366,irc_332,irc_333,irc_329,irc_324 *
    /set weechat.look.prefix_same_nick ">"
    /set weechat.look.prefix_error "⚠"
    /set weechat.look.prefix_network "ℹ "
    /set weechat.look.prefix_action "⚡"
    /set weechat.look.bar_more_down "▼▼"
    /set weechat.look.bar_more_left "◀◀"
    /set weechat.look.bar_more_right "▶▶"
    /set weechat.look.bar_more_up "▲▲"
    /set weechat.look.prefix_suffix "╡"
    /set weechat.look.prefix_align_max 15
    /set weechat.look.buffer_time_format "${253}%H${245}%M"
    /key bind meta-n /bar toggle nicklist


Note that the above symbols require `fontawesome` to be installed.


Oh, and if you happen to idle in large/busy channels and you don’t want to be alerted every time someone talks in there, you can use this command, after adjusting it for the correct server and channel names. This will only show the channel #go-nuts on the freenode IRC network as having activity if someone says your nickname:

    /set weechat.notify.irc.arkham.#ruby highlight

Otherwise there isn't really much else to it.

    ListNetworks after initially connecting to see all ZNC networks available
    JumpNetwork freenode to switch to freenode network
    alt+# to change buffers
    alt+n to show/hide namelist
    pageup/pagedown to digest previous/next messages
    /clear to clear a buffer out
    /join #channel
    /invite <nick> <channel>
    alt+(uparrow|downarrow) to incrementally flip through buffers

![weechat-solarized](/img/weechat-solarized.png)
