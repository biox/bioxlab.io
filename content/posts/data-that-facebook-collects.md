---
title: "Data That Facebook Collects"
date: 2017-06-10T19:08:16-05:00
type: post
---

Before initiating the "permanent deletion" process of my Facebook account, I had
them send me a dump of "data they have collected."

Some of the more interesting tidbits:

![fbdata1](/img/fbdata1.png)
![fbdata2](/img/fbdata2.png)
![fbdata3](/img/fbdata3.png)
![fbdata4](/img/fbdata4.png)
![fbdata5](/img/fbdata5.png)
![fbdata6](/img/fbdata6.png)
![fbdata7](/img/fbdata7.png)
![fbdata8](/img/fbdata8.png)
![fbdata9](/img/fbdata9.png)
![fbdata10](/img/fbdata10.png)
![fbdata11](/img/fbdata11.png)
![fbdata12](/img/fbdata12.png)
![fbdata13](/img/fbdata13.png)
![fbdata14](/img/fbdata14.png)
