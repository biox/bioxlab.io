---
title: "Node Exporter on Openbsd"
date: 2017-08-10T22:55:55-05:00
type: post
---

Jesus eefffffin' atheismo it took all afternoon to figure this out. For the sake of my sanity I need to document it here:

tl;dr: Bloop bloop here's the node_exporter binary for OpenBSD:

## Installing node_exporter on OpenBSD 5.9

First, I want to make it known that everything was tested on openBSD 5.9/6.0. I updated it from 5.7 because I wanted a more recent version of Go.

Anyway, here we go!

We needs to get a few deps from ports to build this project. First, set your PKG_PATH:

    export PKG_PATH=http://ftp.usa.openbsd.org/pub/OpenBSD/`uname -r`/packages/`arch -s` 

Next, download the deps:

    pkg_add go go-tools git go-check-v1

Set up your go_path and get the node_exporter project:

    mkdir /home/username/go
    export GOPATH=/home/username/go
    go get github.com/prometheus/node_exporter

Now we'll need to attempt making the project according to our handy dandy [instructions](https://github.com/prometheus/node_exporter#building-and-running)

    cd /home/username/go/src/github.com/prometheus/node_exporter
    make

Make failed for me for some cryptic reasons:

```
Bad modifier: , ,$(shell $(GO) env GOPATH)))
Bad modifier: , ,$(shell $(GO) env GOPATH)))
*** Parse error in /root/go/src/github.com/prometheus/node_exporter: Missing dependency operator (Makefile:26)
*** Parse error: Need an operator in 'else' (Makefile:28)
*** Parse error: Need an operator in 'endif' (Makefile:30)
*** Parse error: Missing dependency operator (Makefile:32)
*** Parse error: Need an operator in 'else' (Makefile:34)
*** Parse error: Need an operator in 'endif' (Makefile:36)
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
Bad modifier: , ,$(shell GO15VENDOREXPERIMENT=1 go env GOPATH)))
```

Alas, I had to modify the Makefile. I simply removed these lines:

```
ifeq ($(OS),Windows_NT)
    OS_detected := Windows
else
    OS_detected := $(shell uname -s)
endif
ifeq ($(OS_detected), Linux)
    test-e2e := test-e2e
else
    test-e2e := skip-test-e2e
endif
```

However I left the `OS_detected := $(shell uname -s)` line in place.

Make it again:

    make

_BAM_, another failure. This time staticcheck appears to be failing to run. Welp, there's only one way to fix this error, obviously: update Go.

According to this matrix, Go 1.6 available in OpenBSD 6.0's ports.

*[Upgrade](https://duckduckgo.com/?q=undefined+constant.ToInt+go&atb=v70-4_z&ia=web) to OpenBSD [6.0](https://www.openbsd.org/faq/upgrade60.html)!*

And so it goes.

https://ftp4.usa.openbsd.org/pub/OpenBSD/6.0/amd64/

Download bsd.rd and install60.iso

Replace /bsd.rd with the new one. Reboot. Console access required. Blam. Select Upgrade, mount the new OpenBSD CD into the VM, upgrade packages. Reboot. sysmerge. Redo our PKG_ADD and pkg_add -u! Install the new(est) version of go and go-tools. Alright, we should be on the right path now!

Also worth noting: When I SCP'd my bsd.rd to my OpenBSD box it went totally wonkers. I recommend just wget'ing it directly. Or if you're less adventurous you _could_ just boot from the install60.iso image.

Anyway, back to reality.

    pkg_add -u
    go version
    # go-1.6.3

hells yes. As our orange president might say, "it's building time"

```
cd /root/
rm -rf go/
mkdir go
cd go
export GOPATH=/root/go/
go get github.com/prometheus/node_exporter
cd src/github.com/prometheus/node_exporter/
make
# fails
vi Makefile
make
...........
FAILS
```

Turns out my version of make sucks now. So I just hardcoded all of the paths in the Makefile. Ezpz.

    /root/go/bin/promu # instead of the variable version

Almost there...

```
>> building binaries
/root/go/bin/promu build --prefix REFIX
 >   node_exporter
collector/meminfo.go:44: c.getMemInfo undefined (type *meminfoCollector has no field or method getMemInfo)
```

Whatever, it looks like there's a BSD meminfo or some such thing.

    mv collectors/meminfo_bsd.go collectors/meminfo.go

That... worked?!

Wow. A binary. Let's see if she runs.

./node_exporter

holy. crap. It worked.

### I'll do a Prometheus + Grafana blog post eventually.

It'll be far less rambly than this post. Mainly I'm just documenting my BSD woes - which begs the question. Why do I even use OpenBSD?

Because it's cool.

LOVE

<br>

JESSE
