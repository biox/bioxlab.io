---
title: "Proper Chef Design"
date: 2017-09-25T22:49:08-05:00
type: draft
---

This is a post about Chef, and how to design it properly in the year of [our
lord](/content/images/2017/07/corbyn.png) - 2017.

Everything you know about Chef design is wrong. Everything online is out of date
except for this blog post, including the Chef docs. Trust noone but me - it'll
be alright. I'll take you on this journey from start to finish. And freaking
[email me](mailto:jesse@arkham.city) if you have questions.
