---
title: "Alien Covenant"
date: 2017-05-07T23:14:11-05:00
type: post
---

### Leading up to this point

Wow! well. it was about as good as I expected it would be. I'm a sort of an Alien lunatic, and I planned on watching all of the prequels during the week leading up to Covenant. I had just deleted Facebook, my friends in the mass-text I sent were getting rowdy, and I needed to know who was coming. Lo and behold I generated... [this](http://alien.arkham.city).

The buttons no longer function, but they did something like this:
```php
<?php
if(isset($_POST['shit']) ) {
    $data = $_POST['shit'] . "\n";
    $ret = file_put_contents('/usr/share/nginx/html/mondayrsvp.txt', $data, FILE_APPEND | LOCK_EX);
    if($ret === false) {
        die('try again friend');
    }
    else {
        echo "YOU HAVE RSVP'D! THIS IS THE EQUIVALENT TO A BLOOD PACT DO NOT DISAPPOINT ME. ALSO THE PAGE MIGHT TAKE A FEW MINS TO REFRESH.";
    }
}
else {
   die( 'dont accidentally die in a firej');
}
```

_my neck, my back, my flat files pwn your stack._

Anyway, Covenant was awful - and given that my basis of comparison is Sigourney Weaver's Ellen Ripley? Rightfully so. It'd be hard to match. Still though, Covenant should have been called Prometheus 2. The title would have been a lot more suitable based on the content of the film.

#### What was Bothersome?

##### THE FLUTE SCENE

Oh god if there was ever a way to break the viewer clean away from the movie. I take Alien seriously, and using a flute as a metaphor for the difference between being alive and being 'alive'? My mind was on fire. The context had this grim 'ohh i get it now' vibe, but I was distracted by how dramatically they fucked this scene up. Everyone was laughing, the Robots were serious - close, personal facial shots, flute playing, discussing mortality - this was not the horror I came for.

##### THE STUPIDITY

- No helmets? No problem, I rly wanna inhale next to some spores
- Strange eggsac? No problem, I kinda wanna look into it
- Locked my friend in a room and refuse to open the door because I'm paralyzed with fear? No problem, Open the door two minutes later
- Strange Necropolis? No problem, the robot told us it's safe - let's split up
- Ionic storm? No problem, let's fly closer to it and risk 2,000 colonist lives
- Humans and an Alien trapped in hallways that I have absolute control over? No problem, I'll let the Alien die
