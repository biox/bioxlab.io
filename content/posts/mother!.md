---
title: "Mother!"
date: 2017-09-25T22:10:26-05:00
type: post
---

I'm not even sure that Mother! qualifies as a horror film. What it evoked in me
was certainly not horror - it was stress. So. Much. Stress. There's a scene
where you get to hear a babies' neck break. There's a scene where Jennifer
Lawrence is brutally beaten for what seems like several minutes while the camera -
which is temperamental during every other shot - keeps completely still. Baby
neck-snapping and Lawrence-beating aside there was a slowly escalating
atmosphere of loudness. It kept ascending until you were begging for a gap in
the noise. I guess that's sorta the point? If so, it was missed on me.

### Atmosphere

If I had to sum it up - dark, gritty, and tight. Blood-lightbulbs (bloodbulbs)
bursting and a ton of other evil symbolism was parallel with a ton of very human
irks. They really exploited that feeling of "I need to get out of this situation
but it's my house."

![bloobbulb](/img/bloobbulb.png)

### Story

This is always the part where my cynic likes to feed - I just hate most stories.
The most frustrating bit is that 'mother!' strikes that particular place between a
promising story and an okay one. And by that I mean it was promising but grew
slowly mediocre. I would be fine if the movie were just shit throughout, but they
really teased me into a sense that the movie might've been... good.

The first problem that I have with the general story is that the lead (or the
Mother, or Her, or whatever) was abused relentlessly throughout. The only time
she took a stand was after her baby was murdered to death - she then promptly
set fire to her house and had a diamond ripped from her heart. My favorite movie
is Alien because the lead was uncharactaristically strong woman - especially
for the time. Bright, trusting, and full of fire. Clearly that wasn't the
intention here, but I did get the sense that Mother! fell into the horror trope
of making every woman either completely helpless or dead for no good reason.
For example, there was a scene where she was frustrated at her husbando - and he
raped her - and after struggling for precisely ten seconds she was then
overwhelmingly turned on - allegedly by his rape-like tendancies. Eghh.

![idiotapist](/img/idiotapist.png)

_the director, probably_

The circular storyline thing sometimes works, but definitely not here. The
issue in this case is one of consistency. In my mind the story that I witnessed
is pretty much guaranteed to unfold in exactly the same way and left me with
almost no questions. I really like open-endedness and interpretation. The fact
that the content of the story was open to _religious_ or _feminist_ or
_artistic_ or _whatever_ interpretation doesn't imply that the overall story arch is
interpret-able. Clearly this man has been running on this cycle for some time,
and he managed to fuck it up so dramatically during this viewing that I have
severe doubts in his ability to learn or correct his behavior. The smugness of
his smile at the end of the film made me burn. He was all "omg all I needed was
your love to keep on keeping on, surely I'll get it right. Haha isn't this a fun
game." NO. IT'S A TERRIBLE GAME. YOU SHOULD FEEL BAD FOR HAVING THE LEARNING
ABILITY OF AN OCTOPUS OR PROBABLY WORSE.

### Film-o-graphy

Was pretty dizzying most of the time - I think it was meant to add to the
discomfort of the experience. When it was still it was either a nipple
shot or Jennifer getting abused/angry at someone.

![krabs-dizzy](/img/krabs-dizzy.png)

_dizzy shot circa 2017_

![krabs-abuse](/img/krabs-abuse.png)

_abuse shot circa 2017_

### r/im15andthisisdeep

I got the sense that the director intended to plant a bunch of 'woa dude' things
into the film. Like there are a bunch of theories that she's Eve and he's Adam,
but really those theories don't hold a lot of candelabra - he was more of a God
figure. In which case who's Adam? I mean obviously the references were all Cain
and Able-y, clearly a ton of religious parallelism, but none of it really
resolved in anything.

I also really don't understand how people can view the movie as a point of pride
for women - the movie expressed literally nothing that alluded me to believe
that. Jennifer was:

- Drugged
- Raped
- Abused
- Having her baby murdered and eaten
- Called a slut/whore/blahblah
- Burned alive

Idk, maybe if you do some mental gymnastics you could say that the movie
expresses the daily struggle in the life of a woman - but you could say that
about any film at all where the woman is more of a meat-slave than a human.

Anyway, in the end it wound up provoking a lot of thought, but I came out of the
theater pretty disappointed. I'll watch it again after my stress boils down.

            xeee         !8F       oe        .n~~%x.
           d888R         88'     .@88      x88X   888.
          d8888R        :88  ==*88888     X888X   8888L
         @ 8888R       .88F     88888    X8888X   88888
       .P  8888R       :88'     88888    88888X   88888X
      :F   8888R       88F      88888    88888X   88888X
     x"    8888R      .88'      88888    88888X   88888f
    d8eeeee88888eer   d8F       88888    48888X   88888
           8888R     .88        88888     ?888X   8888"
           8888R     d8F        88888      "88X   88*`
        "*%%%%%%**~ :88      '**%%%%%%**     ^"==="`
