---
title: "Lisa the Painful"
date: 2017-04-15T21:02:37-05:00
type: post
---

### LISA the ...

I purchased LISA the Painful after witnessing
[Dunkey](https://www.youtube.com/watch?v=ojapVW-7lQ0) play the game. Based on
his short playthrough it looked clever - and hey, it's available on Linux. Might
as well give it a shot. LISA is a turn-based RPG centered in a mysterious
post-apocalyptic world. There are hoards of sweaty, swearing men - but only one
female. The man who finds this female is named Brad. He names her Buddy and
hides her from the other men.


### Controls

Largely I liked the controls in this game - they're exactly what you'd expect.
Arrow keys/WASD to move around, spacebar/enter to move the dialogue around, ESC
to move the menu around, and Shift to dismount the bicycle (of course). Most
interesting was the _armstrong style_ ability, which allowed you to use **WASD**
keys to perform a generic attack and finish with a powerful ability. It was fun
to memorize the different abilities and eek out that bit of extra damage -
accomplishing in the same way that min-maxing is. 

Some of the player experience nicities were notably missing. For example, after
dismounting the bicycle it was annoying to have to dive through the menu system
to re-mount it. Why not just press shift again? Pokemon was doing this ages ago.
Jumping up and down was also taxing, I found myself wishing that jumping up and
down 'felt more congruent' than it did. The strange arms-out animation of our
hero - Brad - was almost surreal. On a game that prides itself for absurdity I
can almost be compassionate - but jumping animations take it too far for me.


### Sounds

The best and most memorable thing about this game is the sound
design. Seventy tracks of huge importance -
[this](https://youtu.be/_Xu30tYn0D0?t=1h38m40s) track in particular has made
its' way into the bowels of my friend group. It's a noise made after a sneeze,
after disgust, or for fun. It's really incredible work, and I if you take
anything away from this review, it should be that the soundtrack is worth paying
good money for. WORLD CLASS.


### Graphics

Almost as good as the sound, the general design direction of the art is great.
You can smell the joy monsters, your muscles pump up with the weird exercise
cult, Brad looks every bit as disgruntled as is supposed to feel, and generally
there are some memorable pieces of ART. Pictures > words:

![lisa1](/img/lisa1.png)
![lisa2](/img/lisa2.png)
![lisa3](/img/lisa3.png)
![lisa4](/img/lisa4.png)

### Balance

I felt that the balance progression in this game went something like this:

![enron-stock](/img/enron-stock.png)

That jump is the hair-guy boss battle that seemingly happens far
before you're prepared to deal with having one party member annihilated every
turn. At some point I discovered a drunk boi who could spit oil - I also
discovered that this oil combined with Brad's fireball was enough to incinerate everything.
The giant dip into extreme easiness was the drunk and the Brad, and beyond
a few creatures that could bite heads off, the game was very simple to beat from
there. If you're looking before a balance between challenging and
impossible, LISA doesn't strike it well - it's very much all over the place.

### Story

This is my complaint. This is everything wrong with LISA. The story started with
_so much_ aspiration. 

The backdrop of a good story is invisible to the player. In sharp
contrast, I could practically see the story being written by Dingaling. It was
more of a stream of consciousness than a drafted story. Combine that with the
overuse of elipses in the absence of meaningful dialogue, with the humor,
and the constant swearing and you may get the feeling that the story lacks
substance. I was disappointed in the storytelling anyway.
This game could've blown up Undertale-style were it not for the lack of... soul?
As it stands there was not a time that I was legitimately emotional
throughout the game. What's weirder is that everyone seems to praise it for
exactly that reason.

> Marty: "Child... What are those little blue things? Where did you get that?"

> Buddy: "Someone gave them to me... They don't taste good. ... But they make
> you feel good..."

> Marty: "Sweetheart, I don't think you should take those..."

> Buddy: "..."

> Marty: "What? What you lookin at? Who's there? What do you--"

> Brad: "You..."

> Brad: "I can't believe you're still alive."

> Marty: "Bradley..."

> Brad: "...Dad..."

> Brad: "You--"

> Brad: "..."

> Buddy: "No!"

> Buddy: "Brad stop!"

> Buddy: "Don't hurt him! He saved my life!"

> Brad: "Move..."

![lisa6](/img/lisa6.png)

_get familiar with this_

The above dialogue happens during one of the climaxes of the game. Trust me,
it's not emotional with context either. I just couldn't see past the game as a game.

      __     ___  ___
     / /_   / / |/ _ \
    | '_ \ / /| | | | |
    | (_) / / | | |_| |
    \___//_/  |_|\___/
