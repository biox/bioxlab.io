---
title: "Openvpn on Openbsd 5.9"
date: 2017-12-02T11:53:59-06:00
type: post
---

# OPENVPN

OpenVPN has made strides recently, especially with the development of tools
like `Tunnelblick`. My preferred routing platform is still OpenBSD, so I used
it to run OpenVPN. This guide is mostly for me.

I ripped almost all of the commands from this guide: http://www.openbsdsupport.org/openvpn-on-openbsd.html

# Server-side

## Deps

I did everything on OpenBSD 5.9.

    export PKG_PATH=http://ftp.usa.openbsd.org/pub/OpenBSD/`uname -r`/packages/`arch -s`
    pkg_add openvpn
    pkg_add easy-rsa

## Dirs and Keys

We need some certs and keys and dirs.

```
install -m 700 -d /etc/openvpn/private
install -m 700 -d /etc/openvpn/private-client-conf
install -m 755 -d /etc/openvpn/certs
install -m 755 -d /var/log/openvpn
install -m 755 -d /var/openvpn/chrootjail/etc/openvpn
install -m 755 -d /etc/openvpn/chrootjail/etc/openvpn/ccd  # client custom configuration dir
install -m 755 -d /var/openvpn/chrootjail/var/openvpn
install -m 755 -d /var/openvpn/chrootjail/tmp
mv /etc/openvpn/ccd/ /etc/openvpn/crl.pem /var/openvpn/chrootjail/etc/openvpn/
ln -s /var/openvpn/chrootjail/etc/openvpn/crl.pem /etc/openvpn/crl.pem
ln -s /var/openvpn/chrootjail/etc/openvpn/ccd/ /etc/openvpn/
ln -s /var/openvpn/chrootjail/etc/openvpn/replay-persist-file /etc/openvpn/replay-persist-file
ls -alpd /etc/openvpn/certs /etc/openvpn/ccd /var/openvpn/chrootjail /var/openvpn/chrootjail/etc /var/openvpn/chrootjail/etc/openvpn /etc/openvpn/private /etc/openvpn/private-client-conf /var/log/openvpn
```

Output should look something like this:

```
lrwxr-xr-x  1 root  wheel   40 Dec  2 12:02 /etc/openvpn/ccd -> /var/openvpn/chrootjail/etc/openvpn/ccd/
drwxr-xr-x  2 root  wheel  512 Dec  2 11:56 /etc/openvpn/certs/
drwx------  2 root  wheel  512 Dec  2 11:56 /etc/openvpn/private/
drwx------  2 root  wheel  512 Dec  2 11:56 /etc/openvpn/private-client-conf/
drwxr-xr-x  2 root  wheel  512 Dec  2 11:56 /var/log/openvpn/
drwxr-xr-x  4 root  wheel  512 Dec  2 12:02 /var/openvpn/chrootjail/
drwxr-xr-x  3 root  wheel  512 Dec  2 12:02 /var/openvpn/chrootjail/etc/
drwxr-xr-x  2 root  wheel  512 Dec  2 12:02 /var/openvpn/chrootjail/etc/openvpn/
```

Verify easy-rsa

    cd /usr/local/share/easy-rsa/
    ls -alpd easyrsa vars*
    less vars.example

_Ensure that vars.example meets your needs._

Generate some keys

    pkiDir="/etc/openvpn/easy-rsa-pki/"
    mkdir ${pkiDir}
    easyrsaDir="/usr/local/share/easy-rsa/"
    cd ${easyrsaDir}
    ls -alp /etc/openvpn/private/vpn-ta.key || openvpn --genkey --secret /etc/openvpn/private/vpn-ta.key
    ./easyrsa --batch=0 --pki-dir=${pkiDir} init-pki # creates empty dirs ${pkiDir}/ ${pkiDir}/private/ ${pkiDir}/reqs/ . batch=1 - overwrite/delete without asking
    ls -alpd ${pkiDir} ${pkiDir}/*
    ./easyrsa --batch=1 --pki-dir=${pkiDir} gen-dh
    ls -alpd ${pkiDir}/dh.pem
    ./easyrsa --batch=1 --pki-dir=${pkiDir} --req-cn=vpn-ca build-ca nopass
    ls -alpd ${pkiDir}/ca.crt ${pkiDir}/private/ca.key ${pkiDir}/index.txt ${pkiDir}/serial
    openssl x509 -in ${pkiDir}/ca.crt -text -noout
    openssl rsa -in ${pkiDir}/private/ca.key -check -noout
    ./easyrsa --batch=1 --pki-dir=${pkiDir} --req-cn=vpnserver gen-req vpnserver nopass
    ls -alpd ${pkiDir}/reqs/vpnserver.req ${pkiDir}/private/vpnserver.key
    openssl req -in ${pkiDir}/reqs/vpnserver.req -text -noout
    openssl rsa -in ${pkiDir}/private/vpnserver.key -check -noout
    ./easyrsa --batch=1 --pki-dir=${pkiDir} show-req vpnserver
    ./easyrsa --batch=1 --pki-dir=${pkiDir} sign server vpnserver # server - only for server
    ls -alpd ${pkiDir}/issued/vpnserver.crt ${pkiDir}/certs_by_serial/01.pem 
    openssl x509 -in ${pkiDir}/issued/vpnserver.crt -text -noout
    echo "Last added cert in db: `cat ${pkiDir}/index.txt|tail -1`"
    echo "Next added cert will have number: `cat ${pkiDir}/serial`"
    ./easyrsa --batch=1 --pki-dir=${pkiDir} gen-crl
    # make sure openvpn process can read this file - otherwise it will crash
    chown :_openvpn ${pkiDir}/crl.pem; chmod g+r ${pkiDir}/crl.pem 
    ls -alF ${pkiDir}/crl.pem
    openssl crl -in ${pkiDir}/crl.pem -text -noout
    cp -p ${pkiDir}/ca.crt /etc/openvpn/certs/vpn-ca.crt
    cp -p ${pkiDir}/private/ca.key /etc/openvpn/private/vpn-ca.key
    cp -p ${pkiDir}/issued/vpnserver.crt /etc/openvpn/certs/vpnserver.crt
    cp -p ${pkiDir}/private/vpnserver.key /etc/openvpn/private/vpnserver.key
    cp -p ${pkiDir}/dh.pem /etc/openvpn/dh.pem
    cp -p ${pkiDir}/crl.pem /etc/openvpn/crl.pem
    openssl x509 -in /etc/openvpn/certs/vpn-ca.crt -text -noout
    openssl x509 -in /etc/openvpn/certs/vpnserver.crt -text -noout
    openssl crl -in /etc/openvpn/crl.pem -text -noout
    openssl rsa -in /etc/openvpn/private/vpn-ca.key -check -noout
    openssl rsa -in /etc/openvpn/private/vpnserver.key -check -noout

Setup passwords for management interface

```
test -f /etc/openvpn/private/mgmt.pwd || touch /etc/openvpn/private/mgmt.pwd
chown root:wheel /etc/openvpn/private/mgmt.pwd; chmod 600 /etc/openvpn/private/mgmt.pwd
vi /etc/openvpn/private/mgmt.pwd  # insert one line of text with clear-text password for management console
```

Setup OpenVPN Server config

    test -f /etc/openvpn/server_tun0.conf || touch /etc/openvpn/server_tun0.conf
    chown root:_openvpn /etc/openvpn/server_tun0.conf; chmod 640 /etc/openvpn/server_tun0.conf

vi /etc/openvpn/server_tun0.conf  # setup OpenVpn server

    ca /etc/openvpn/certs/vpn-ca.crt  # CUSTOMIZED by adminOfMine #
    cert /etc/openvpn/certs/vpnserver.crt  # CUSTOMIZED by adminOfMine #
    key /etc/openvpn/private/vpnserver.key  # CUSTOMIZED by adminOfMine #
    dh /etc/openvpn/dh.pem  # CUSTOMIZED by adminOfMine #
    ifconfig-pool-persist /var/openvpn/ipp.txt  # CUSTOMIZED by adminOfMine # put it to var as it is dynamic file
    tls-auth /etc/openvpn/private/vpn-ta.key 0 # This file is secret  # CUSTOMIZED by adminOfMine #
    replay-persist /etc/openvpn/replay-persist-file  # CUSTOMIZED by adminOfMine #
    max-clients 100  # CUSTOMIZED by adminOfMine #
    status /var/log/openvpn/openvpn-status.log  # CUSTOMIZED by adminOfMine #
    log-append  /var/log/openvpn/openvpn.log  # CUSTOMIZED by adminOfMine #
    proto tcp  #or use udp if works better # CUSTOMIZED by adminOfMine #
    port 1194 # CUSTOMIZED by adminOfMine #
    management 127.0.0.1 1195 /etc/openvpn/private/mgmt.pwd  # CUSTOMIZED by adminOfMine #
    daemon openvpn  # CUSTOMIZED by adminOfMine #
    chroot /var/openvpn/chrootjail  # CUSTOMIZED by adminOfMine #
    crl-verify /etc/openvpn/crl.pem  # CUSTOMIZED by adminOfMine #
    float        # CUSTOMIZED by adminOfMine #
    persist-key  # CUSTOMIZED by adminOfMine #
    persist-tun  # CUSTOMIZED by adminOfMine #
    #Additional authorization options - needs to be configured - read further before enabling this
    ;auth-user-pass-verify /var/openvpn/custom-simple-auth via-env  # CUSTOMIZED by adminOfMine #
    ;script-security 3  # CUSTOMIZED by adminOfMine #
    # The following options were set by default 
    keepalive 10 120
    comp-lzo
    user _openvpn
    group _openvpn
    verb 3
    # If you would prefer to start openvpn directly and NOT via /etc/hostname - then you need to edit and uncomment those parameters
    ;dev tun0  # CUSTOMIZED by admin-of-mine #
    ;server 10.8.0.0 255.255.255.0  # Address range for the tun(4)interfaces  # CUSTOMIZED by adminOfMine #
    # Add a route to the local network to the client's routing table
    ;push "route 192.168.0.0 255.255.255.0"  # CUSTOMIZED by adminOfMine #
    # If you want to push DNS server setting to VPN clients - uncomment below and see openvpn documentation for more info
    ;push "dhcp-option DNS 192.168.0.x"   # CUSTOMIZED by adminOfMine #
    # If you use samba and want to have wins (windows netbios) name resolution then add WINS (samba nmbd) server IP
    ;push "dhcp-option WINS 192.168.0.x"  # CUSTOMIZED by adminOfMine #

Edit firewall rules

```
    log_opt = "log" # Set to ="log" to enable pflog for rulesfiles that use this macro. Setting to ="" will do no logging to pflog

    int_if="em0"
    tun_if="tun0"   # open-vpn interface
    ext_if="re0"    # external connections are forwarded to this interface
    allowIncoming_ifs="{" $int_if $tun_if "}"  # interfaces where (internal) incoming traffic is allowed

    table <myself> const { self }
    table <myBroadcast> const { self:broadcast }
    table <private> const { 10/8, 172.16/12, 192.168/16 }
    table <internet> const {0.0.0.0/0, !10/8, !172.16/12, !192.168/16 }

    vpnNetwork="10.8/16"
    table <vpnNet> const { $vpnNetwork }
    table <allowedIncomingLan> const { self:network $vpnNetwork }
    table <allowedIncomingExternal> const { 0/0 }   # these will be public IPs from the outside

    SpecialAddressReachableViaVpn="192.168.99.xx"
    lan_tcp_services="{ 22, 53, 80, 139, 445, 113, 1194 }"
    lan_udp_services="{ 53, 137:138,  1194 }"

    #... other rules, scrub, antispoof, etc.

## From LAN and VPN  to OPENVPN-SERVER -- INCOMING
   # allow incoming TCP to certain services - including OPENVPN port 1194
    pass  in  quick $log_opt on $allowIncoming_ifs proto tcp from <allowedIncomingLan> to <myself> port $lan_tcp_services  label "incomingToHost"
   # allow incoming UDP to certain services - including OPENVPN port 1194:
    pass  in  quick $log_opt on $allowIncoming_ifs proto udp from <allowedIncomingLan> to <myself> port $lan_udp_services  label "incomingToHost"
    pass  in  quick $log_opt on $allowIncoming_ifs proto udp from <allowedIncomingLan> to <myBroadcast> port { 137:138 }   # netbios name/datagram broadcasts
   # allow incoming ping
    pass  in  quick $log_opt on $allowIncoming_ifs inet proto icmp from { <allowedIncomingLan> } to <myself> icmp-type 8 code 0  label "icmp-in"
   # anything else is explicitly blocked here (so that rules allowing for routing and vpn will not allow what is not wanted)
    # block access to this host - whatever was not matched earlier
    block in  quick $log_opt on $allowIncoming_ifs from { <allowedIncomingLan> } to <myself>


## From LAN (VPN) to LAN  --  TRAVERSING - INBOUND
   # allow VPN access the LAN
    # Note: there is no network defined on $tun_if, so cannot use syntax like: pass ... from ($tun_if:network:0) to ...
    pass  in  quick $log_opt on $tun_if from <vpnNet> to ($int_if:network) label "vpn-in"
    pass  in  quick $log_opt on $tun_if from <vpnNet> to $SpecialAddressReachableViaVpn label "vpn-in-special"


## From OPENVPN-SERVER -- OUTGOING  - and -  From LAN (VPN) to LAN  --  TRAVERSING - OUTBOUND
   # allow and do NAT for VPN - the VPN clients will connect to other hosts in internal network using IP of this host as source IP
   # nat is not needed if the openvpnserver is the default router for all computers on LAN.
    pass  out quick $log_opt on $int_if to ($int_if:network) received-on $tun_if nat-to ($int_if:0) label "vpn-out-to-network"

   # INTERNET -- OUTGOING - allow LAN hosts to reach internet, etc. Do modulate state for tcp.
    ##match out on egress inet from !(egress:network) to any nat-to (egress:0)
    pass  out quick $log_opt on $ext_if from <hostsAllowedToAccessInternet> to <internet> nat-to ($ext_if:0) modulate state label "lan-out-to-internet"
    pass  out quick $log_opt on $tun_if from <vpnNet> to $SpecialAddressReachableViaVpn label "vpn-out-special"


## INTERNET -- INCOMING -- CAREFULLY
   # allow incoming connection from the internet - allow to connect to OPENVPN on UDP and TCP
    pass  in  quick $log_opt on $allowIncomingExternal_ifs proto udp from <allowedIncomingExternal> to <myself> port { 1194 } label "incoming-external:$srcaddr"
    pass  in  quick $log_opt on $allowIncomingExternal_ifs proto tcp from <allowedIncomingExternal> to <myself> port { 1194 } label "incoming-external:$srcaddr"
    block $log_opt
```

Start it up

    /usr/local/sbin/openvpn --daemon --config /etc/openvpn/server_tun0.conf --dev tun0 --server 10.8.0.0 255.255.255.0 --push "route 192.168.0.0 255.255.255.0"
