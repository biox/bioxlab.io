---
title: "Election Thoughts"
date: 2016-11-04T23:16:49-05:00
type: post
---

_Originally posted the night before the 2016 election_

As an avid horror fan, there's a certain charm to this years election.


## The Donald
We have a famous orange builder, who denounces political culture (ironically
he's obsessed with race and gender politics). Donald Trump is viewed in an
extremely negative light in almost every social circle I'm a part of. He's
boisterous, loud, and to the point. Another irony is that he's misunderstood.

Widely perceived as:

- [uncharitable](https://www.looktothestars.org/celebrity/donald-trump)
- [sexist](https://www.washingtonpost.com/politics/donald-trump-a-champion-of-women-his-female-employees-think-so/2015/11/23/7eafac80-88da-11e5-9a07-453018f9a0ec_story.html)
- and [stupid](https://www.quora.com/How-smart-is-Donald-Trump) _yes this is on
  quora, but it's actually an incredible read._


  The above isn't definitive - Donald Trump isn't the most charitable man alive.
  He's also definitely sexist, and arguably stupid (in a certain sense). The
  problem is that the above statements - taken at blanket-statement value, don't
  hold weight. On a sliding scale, Donald is probably somewhere in the middle.

  If I had inherited fourteen million dollars from my father, I probably would
  have died of a heroin overdose at fifteen. Trump turned himself into a
  multi-billionaire. He owns a name that's globally recognized - he has created
  a legacy that will far surpass him. His children are likely to enter into
  politics as real contenders, even if Donald fails to claim the presidency.
  Clearly Donald deserves more credit than most people give him.

  All said, I don't think that he's a bad person in general. But I also don't
  believe that he's deserving of the presidency. In fact, I'd wager that Trump
  has a lot more in common with the average American than most politicians.

  The sad truth is that the average American isn't good enough. Too racist,
  sexist, and nationalistic to operate on the global stage that is supposed to
  represent our essence.

  Clearly there's a reason Donald is doing well. He's a populist with a grenade
  launcher. The grenade is modern politics. Everyone I speak with (largely a
  liberal crowd) is disillusioned with politics. There is a very real concern in
  America. Between Bill Clinton's infidelities, Bush's war declaration, and
  Obama's drone-strikes (and two billion dollar website) there's a feeling that
  the very system that is supposed to be our foundation is working against us.

  Trump may not win, but this feeling will not go away unless the foundation
  shifts. Do you think Trump is scary? Just wait. The United States doesn't yet
  have a Marine Le Pen, but we will.

  This is also why I'm discontent with Hillary Clinton.


## Hilldawg
Hillary Clinton represents the status quo - a continuation of Obama-era
politics. This would be fine, maybe even *GREAT*, except. Well. You know.

http://www.npr.org/2016/06/12/481718785/clinton-scandals-a-guide-from-whitewater-to-the-clinton-foundation

All of the above without mentioning Veritas, or the way that her campaign shit
[all
over](http://www.breitbart.com/2016-presidential-race/2016/10/26/wikileaks-clinton-team-leaked-creepshot-of-bernie-sanders-in-his-swimming-suit/)
Bernie, the soul of new voters everywhere. (yes, I realize that's Breitbart. No,
I'm not endorsing them)

My point is not that Hillary Clinton is deceitful and plays the system to her
benefit - **and we see that**. My point is that despite whether or not you
believe she did, the impression among both Democrats and Republicans is that she
is generally untrustworthy, shady, and a perfect representation of everything
that America as a whole _despises_ about politics.

The flames are stoked, and the fire of a Clinton presidency has the potential to
birth a new populist breed of which Donald is merely an ancestor. I have a
feeling that, if Hillary is elected, in a certain sense we're dooming ourselves
to a worse Trump further down the line.


## Third parties don't matter
And they don't matter in this election either. Despite what your
anarcho-capitalist friends or Green party sympathizers might tell you. If The
highest legislative office ever held by a Libertarian who won office as a member
of the Libertarian Party was [state
representative](https://www.quora.com/What-is-the-highest-office-ever-held-by-a-Libertarian-in-the-US),
and the Green Party has a single [mayor](http://www.gp.org/officeholders), you
have to admit that shooting for president is more of an advertising scheme than
a realistic opportunity.

Local elections matter. Elect a green/libertarian governor first, then maybe the
presidency won't seem so infeasibly stupid.

I identify as a socialist, but I'm probably holding my nose and voting for
Clinton because _President_ is not a reasonable office for the socialist
movement to start in. We need to start from the grassroots.


## So where does that leave us?
Hell if I know. I'm heading to the voting booth with a set of precautions. A
mouthful of amnesiacs, one bucket, and eyes stained with tears over the death of
our greatest political movement in decades - squandered by the DNC.

Good luck out there.

_2017 UPDATE: Bernie would have won._
